# K8s demo

This is a demo script that is part of the Linux Web Services course at Thomas More University of Applied Sciences. It shows how to quickly set up a Kubernetes cluster using KinD and deploy a simple nginx hello world app to it. It also shows how to simulate a failure of a worker node and how to scale out the deployment.

## Clone this repo

```bash
$ git clone https://gitlab.com/it-factory-thomas-more/linux-web-services/course/linux-ws-k8s-demo.git
$ cd linux-ws-k8s-demo
```


## Install kind if needed

```bash
$curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11/kind-linux-amd64
$chmod +x ./kind
$sudo mv ./kind /usr/local/bin/kind
```


## Setup up cluster and deploy app

 Deploy KinD k8s cluster with1 controller and 2 worker nodes

```bash
$kind create cluster --config=./kindconfig
$kubectl get nodes
NAME                             STATUS   ROLES           AGE   VERSION
intro-class-demo-control-plane   Ready    control-plane   14h   v27.1
intro-class-demo-worker          Ready    <none>          14h   v27.1
intro-class-demo-worker2         Ready    <none>          14h   v27.1
```

 Deploy ingress load balancer

```bash
$kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml
```

Wait until fully deployed

```bash
$kubectl get pods -n ingress-nginx 
NAME                                        READY   STATUS      RESTARTS   AGE
ingress-nginx-admission-create-247s4        0/1     Completed   0          82s
ingress-nginx-admission-patch-6n8tz         0/1     Completed   2          82s
ingress-nginx-controller-5bb6b499dc-zvqkb   1/1     Running     0          82s
```

 Now, modify ingress host in nginx-hello.yaml to your test domain name (use messwithdns.net or /etc/hosts)
 Finally, deploy nginx hello world app to k8s
```bash
$kubectl apply -f nginx-hello.yaml
```

 Browse to domain name on port 80 -> should see nginx hello world app -> Refresh -> Always same IP and server name

```bash
$kubectl get pod -o wide                
NAME                                READY   STATUS    RESTARTS   AGE     IP           NODE                       NOMINATED NODE   READINESS GATES
hello-deployment-5bd6dd7dff-swm6b   1/1     Running   0          4m30s   10.244.7   intro-class-demo-worker2   <none>           <none>
```

# Simulate failure 

Now, let's simulate a failure of the worker node the app is running on.

 The following command will drain all workloads from the worker and cordon the worker so that it will not process any new workloads. Fill in X with active worker node.

```bash
$kubectl drain intro-class-demo-workerX --ignore-daemonsets

$kubectl get pod -o wide                                   
NAME                                READY   STATUS    RESTARTS   AGE   IP           NODE                      NOMINATED NODE   READINESS GATES
hello-deployment-5bd6dd7dff-m7nn2   1/1     Running   0          10s   10.244.2.7   intro-class-demo-worker   <none>           <none>
```
 Test using browser -> app is still running. IP and server name changed (new container on other worker).

We see that the app has moved to the other worker node. This proves fault tolerance.

# Horizontal scaling

 Let's uncordon the worker again. This way we can show load balancing and distributing workloads. Fill in X with previously drained node.
```bash
$kubectl uncordon intro-class-demo-workerX
node/intro-class-demo-worker2 uncordoned
```

 The following command will scale the deployment out.
```bash
$kubectl scale deployment hello-deployment --replicas 3
deployment.apps/hello-deployment scaled
```
The app container is now distributed across 2 workers
```bash
$kubectl get pod -o wide                               
NAME                                READY   STATUS    RESTARTS   AGE   IP           NODE                       NOMINATED NODE   READINESS GATES
hello-deployment-5bd6dd7dff-8pcxm   1/1     Running   0          3s    10.244.8   intro-class-demo-worker2   <none>           <none>
hello-deployment-5bd6dd7dff-bk8gc   1/1     Running   0          3s    10.244.9   intro-class-demo-worker2   <none>           <none>
hello-deployment-5bd6dd7dff-m7nn2   1/1     Running   0          14m   10.244.2.7   intro-class-demo-worker    <none>           <none>
```

 Test using browser -> refresh -> every time different IP and server name 
This proves that we can easily distribute workloads across multiple servers!


# Cleanup

 Delete kind cluster after demo

```bash
#Delete kind cluster after demo
$kind delete clusters intro-class-demo
Deleted nodes: ["intro-class-demo-worker2" "intro-class-demo-control-plane" "intro-class-demo-worker"]
Deleted clusters: ["intro-class-demo"]
```

